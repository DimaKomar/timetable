class CreateAccs < ActiveRecord::Migration
  def change
    create_table :accs do |t|
      t.string :username

      t.timestamps null: false
    end
  end
end
