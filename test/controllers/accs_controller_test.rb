require 'test_helper'

class AccsControllerTest < ActionController::TestCase
  setup do
    @acc = accs(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:accs)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create acc" do
    assert_difference('Acc.count') do
      post :create, acc: { username: @acc.username }
    end

    assert_redirected_to acc_path(assigns(:acc))
  end

  test "should show acc" do
    get :show, id: @acc
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @acc
    assert_response :success
  end

  test "should update acc" do
    patch :update, id: @acc, acc: { username: @acc.username }
    assert_redirected_to acc_path(assigns(:acc))
  end

  test "should destroy acc" do
    assert_difference('Acc.count', -1) do
      delete :destroy, id: @acc
    end

    assert_redirected_to accs_path
  end
end
